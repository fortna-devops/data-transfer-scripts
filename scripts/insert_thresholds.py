# Use mhs-rdb version v1.1.2

import argparse
import csv
from datetime import datetime
import os
import pprint
from typing import List
from collections import OrderedDict
import boto3
from sqlalchemy.orm import contains_eager
import xlrd
from mhs_rdb import Thresholds, ConfigManager, Sensors #Assets


os.environ['env'] = 'master'
os.environ['AWS_RDB_USERNAME'] = 'developer'
os.environ['debug'] = 'TRUE'

CONFIG_MANAGER = ConfigManager(
    env=os.environ['env'],
    boto_session=boto3.Session(profile_name=os.getenv('profile')),
    debug=os.environ.get('debug') == 'TRUE'
)


def get_all_thresh(session):
    return session\
        .query(
            Thresholds.model.id,
            Thresholds.model.sensor_id,
            Thresholds.model.type,
            Thresholds.model.metric,
            # Thresholds.model.level,
            Thresholds.model.name,
            Thresholds.model.value,
            Thresholds.model.created_at,
            Thresholds.model.updated_at)\
        .all()


def excel_to_csv(excel_file: str) -> None:
    """
    Convert the given excel file to a csv and store in PATH_TO_CSV
    """
    wb = xlrd.open_workbook(excel_file)
    sh = wb.sheet_by_name('Thresholds')
    csv_file = open('tmp.csv', 'w')  # TODO: Delete this file afterwards.
    wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    csv_file.close()


def get_thresholds_info() -> List[OrderedDict]:
    file_path = 'tmp.csv'
    data = []
    with open(file_path, 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar='"')
        data = [row for row in reader]
    return data


def upsert_thresholds(csv_data: List[OrderedDict], customer: str, facility: str) -> None:
    dt = datetime.utcnow()
    site = f"{customer}_{facility.replace('-', '_')}"

    with CONFIG_MANAGER.connector.get_session(customer, site) as db_session:

        thresh_res = get_all_thresh(db_session)
        sensors_res = Sensors(db_session).get_all()
        thresholds = {(threshold_row.sensor_id, threshold_row.type.name, threshold_row.metric.name, threshold_row.name): threshold_row for threshold_row in thresh_res}
        sensors = {(sensor.conveyor.name, sensor.equipment.name): sensor for sensor in sensors_res}


        for row in csv_data:
            thresh_type = row.get('type')
            metric = row.get('metric')
            name = row.get('color')
            value = row.get('threshold_value')
            level = row.get('level')
            equipment_type = row.get('equipment_type')
            conveyor_name = None
            if row.get('conveyor_name'):
                conveyor_name = row.get('conveyor_name').replace('\xa0', '')
                if conveyor_name[-1] == '0':
                    conveyor_name = row.get('conveyor_name').replace('\xa0', '').split('.')[0]

            sensor_id = None
            sensor_unique_id = (conveyor_name, equipment_type)
            sensor_id = sensors[sensor_unique_id].id

            threshold_id = None
            thresh_unique_id = (sensor_id, thresh_type, metric, name)
            threshold = thresholds.get(thresh_unique_id)

            if threshold:
                Thresholds(db_session).update(
                    Thresholds.model.id == threshold.id,
                    values={'value': value, 'updated_at': dt})

            else:
                Thresholds(db_session).insert(
                    Thresholds.model(
                        sensor_id=sensor_id,
                        type=thresh_type,
                        metric=metric,
                        level=level,
                        name=name,
                        value=value,
                        created_at=dt))


def main(excel, customer, facility):
    excel_to_csv(excel)
    thresholds_info = get_thresholds_info()
    upsert_thresholds(thresholds_info, customer, facility)

    os.remove('tmp.csv')


if __name__ == '__main__':
    os.environ['env'] = 'master'
    os.environ['AWS_RDB_USERNAME'] = 'developer'
    os.environ['debug'] = 'TRUE'

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('customer', action='store', help='Customer name')
    arg_parser.add_argument('facility', action='store', help='Facility (site) name')
    args = arg_parser.parse_args()
    args = vars(args)

    customer = args['customer']
    facility = args['facility']

    from pathlib import Path
    full_path = os.path.abspath("insert_thresholds.py")
    excel = f'{str(Path(full_path).parents[1])}/csvs/data2insert/{customer}-{facility}/thresholds.xlsx'

    main(excel, customer, facility)
